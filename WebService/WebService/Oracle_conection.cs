﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.OracleClient;
using System.Web.Services;

namespace WebService
{
    public class Oracle_conection
    {
        private OracleConnection oc;
        string oradb = "DATA SOURCE=localhost:1521/orcl;PERSIST SECURITY INFO=True;USER ID=DBFEMTAXI; PASSWORD=123456789";
        public Oracle_conection()
        {
        }

        public void EstablecerConnection()
        {
            oc = new OracleConnection(oradb);
            oc.Open();

        }

        public OracleConnection GetConexion()
        {
            return oc;
        }
    }
}