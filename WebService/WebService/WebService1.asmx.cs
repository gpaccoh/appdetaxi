﻿using System;
using System.Collections.Generic;
using System.Data.OracleClient;
using System.Diagnostics;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Data;
using System.Web.Services;



namespace WebService
{
    /// <summary>
    /// Descripción breve de WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            Oracle_conection oracle = new Oracle_conection();
            oracle.EstablecerConnection();
            OracleConnection conn = oracle.GetConexion();
            OracleCommand cmd = new OracleCommand();
            cmd.CommandText = "select * from usuario";
            cmd.Connection = conn;

            OracleDataReader dr = cmd.ExecuteReader();
            string resultado = "";
            //cmd.ExecuteNonQuery()
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Debug.WriteLine(dr["nombre_usuario"].ToString());
                    resultado += dr["nombre_usuario"] + "\n";
                }
            }
            else
            {
                Debug.Write("No Data In DataBase");
            }
            conn.Close();
            return resultado;
        }

        [WebMethod(BufferResponse = true)]
        public object Insertar_Usuario(string nombre, string apellido, string direccion, int telefono, string correo)

        {
            Oracle_conection conn = new Oracle_conection();
            conn.EstablecerConnection();

            OracleParameter param_nombre = new OracleParameter();
            param_nombre.OracleType = OracleType.VarChar;
            param_nombre.Value = nombre;

            OracleParameter param_apellido = new OracleParameter();
            param_apellido.OracleType = OracleType.VarChar;
            param_apellido.Value = apellido;

            OracleParameter param_direccion = new OracleParameter();
            param_direccion.OracleType = OracleType.VarChar;
            param_direccion.Value = direccion;

            OracleParameter param_telefono = new OracleParameter();
            param_telefono.OracleType = OracleType.VarChar;
            param_telefono.Value = telefono;

            OracleParameter param_correo = new OracleParameter();
            param_correo.OracleType = OracleType.VarChar;
            param_correo.Value = correo;


            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn.GetConexion();
            cmd.CommandText = "Insertar_Usuario";
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            //cmd.Parameters.Add("P1", OracleType.Number).Value = param_id.Value;
            cmd.Parameters.Add("nombre", OracleType.VarChar).Value = param_nombre.Value;
            cmd.Parameters.Add("apellido", OracleType.VarChar).Value = param_apellido.Value;
            cmd.Parameters.Add("direccion", OracleType.VarChar).Value = param_direccion.Value;
            cmd.Parameters.Add("telefono", OracleType.VarChar).Value = param_telefono.Value;
            cmd.Parameters.Add("correo", OracleType.VarChar).Value = param_correo.Value;
            cmd.ExecuteNonQuery();
            return "Usuario insertado correctamente";
            //return pc.Clientes_New_Pro(conn.GetConexion(), id, nombre, apellido ,direccion, telefono,correo);
        }
        [WebMethod(BufferResponse = true)]
        public Object Aumentar_monto_pedido()
        {
            Oracle_conection conn = new Oracle_conection();
            conn.EstablecerConnection();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn.GetConexion();
            cmd.CommandText = "Aumentar_monto_pedido";
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
            return "monto_aumentado";
        }
        [WebMethod(BufferResponse = true)]
        public string Usuario_Select_Full()
        {
            Oracle_conection conn = new Oracle_conection();
            conn.EstablecerConnection();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn.GetConexion();

            OracleParameter param_name = new OracleParameter();
            param_name.OracleType = OracleType.Cursor;
            cmd.CommandText = "mostrar_usuario";
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add("c_cursor", OracleType.Cursor).Direction = ParameterDirection.Output;
            DataSet ds = new DataSet();
            OracleDataAdapter da = new OracleDataAdapter(cmd);
            da.Fill(ds);
            return JsonConvert.SerializeObject(ds, Newtonsoft.Json.Formatting.Indented);
        }
        [WebMethod(BufferResponse = true)]
        public string Pedidos_Select_Full()
        {
            Oracle_conection conn = new Oracle_conection();
            conn.EstablecerConnection();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn.GetConexion();

            OracleParameter param_name = new OracleParameter();
            param_name.OracleType = OracleType.Cursor;
            cmd.CommandText = "mostrar_pedidos";
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add("c_cursor", OracleType.Cursor).Direction = ParameterDirection.Output;
            DataSet ds = new DataSet();
            OracleDataAdapter da = new OracleDataAdapter(cmd);
            da.Fill(ds);
            return JsonConvert.SerializeObject(ds, Newtonsoft.Json.Formatting.Indented);
        }

        [WebMethod]
        public int Add(int x, int y)
        {
            return x + y;
        }
        [WebMethod]
        public int Sub(int x, int y)
        {
            return x - y;
        }
        [WebMethod]
        public int Mul(int x, int y)
        {
            return x * y;
        }
        [WebMethod]
        public int Div(int x, int y)
        {
            return x / y;
        }
        [WebMethod(BufferResponse = true)]
        public object Actualizar_Usuario(int id, string nombre, string apellido, string direccion, int telefono, string correo)

        {
            Oracle_conection conn = new Oracle_conection();
            conn.EstablecerConnection();

            OracleParameter param_id = new OracleParameter();
            param_id.OracleType = OracleType.Number;
            param_id.Value = id;

            OracleParameter param_nombre = new OracleParameter();
            param_nombre.OracleType = OracleType.VarChar;
            param_nombre.Value = nombre;

            OracleParameter param_apellido = new OracleParameter();
            param_apellido.OracleType = OracleType.VarChar;
            param_apellido.Value = apellido;

            OracleParameter param_direccion = new OracleParameter();
            param_direccion.OracleType = OracleType.VarChar;
            param_direccion.Value = direccion;

            OracleParameter param_telefono = new OracleParameter();
            param_telefono.OracleType = OracleType.VarChar;
            param_telefono.Value = telefono;

            OracleParameter param_correo = new OracleParameter();
            param_correo.OracleType = OracleType.VarChar;
            param_correo.Value = correo;


            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn.GetConexion();
            cmd.CommandText = "Actualizar_Usuario";
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add("idu", OracleType.Number).Value = param_id.Value;
            cmd.Parameters.Add("nombre", OracleType.VarChar).Value = param_nombre.Value;
            cmd.Parameters.Add("apellido", OracleType.VarChar).Value = param_apellido.Value;
            cmd.Parameters.Add("direccion", OracleType.VarChar).Value = param_direccion.Value;
            cmd.Parameters.Add("telefono", OracleType.VarChar).Value = param_telefono.Value;
            cmd.Parameters.Add("correo", OracleType.VarChar).Value = param_correo.Value;
            cmd.ExecuteNonQuery();
            return "Usuario actualizado  correctamente";
            //return pc.Clientes_New_Pro(conn.GetConexion(), id, nombre, apellido ,direccion, telefono,correo);
        }

        [WebMethod(BufferResponse = true)]
        public string Eliminar_Usuario(int id)
        {
            Oracle_conection conn = new Oracle_conection();
            conn.EstablecerConnection();
            OracleParameter param_id = new OracleParameter();
            param_id.OracleType = OracleType.Number;
            param_id.Value = id;

            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn.GetConexion();
            cmd.CommandText = "Eliminar_Usuario";
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add("idu", OracleType.Number).Value = param_id.Value;
            cmd.ExecuteNonQuery();
            return "Usuario eliminado correctamente";
        }


        [WebMethod(BufferResponse = true)]
        public int Login(String usuario, String contraseña)
        {
            Oracle_conection conn = new Oracle_conection();
            conn.EstablecerConnection();

            OracleParameter param_usu = new OracleParameter();
            param_usu.OracleType = OracleType.VarChar;
            param_usu.Value = usuario;

            OracleParameter param_contra = new OracleParameter();
            param_contra.OracleType = OracleType.VarChar;
            param_contra.Value = contraseña;

            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn.GetConexion();
            cmd.CommandText = "login_usu";

            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add("c_usuario", OracleType.VarChar).Value = param_usu.Value;
            cmd.Parameters.Add("c_contra", OracleType.VarChar).Value = param_contra.Value;
            cmd.Parameters.Add("n_resp", OracleType.Number).Direction = System.Data.ParameterDirection.Output;
            cmd.ExecuteNonQuery();
            int res = int.Parse(cmd.Parameters["n_resp"].Value.ToString());
            //return "Usuario eliminado correctamente";
            return res;

        }


        [WebMethod(BufferResponse = true)]
        public string Eliminar_Pedido(int id)
        {
            Oracle_conection conn = new Oracle_conection();
            conn.EstablecerConnection();
            OracleParameter param_id = new OracleParameter();
            param_id.OracleType = OracleType.Number;
            param_id.Value = id;

            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn.GetConexion();
            cmd.CommandText = "Eliminar_Pedido";
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add("idp", OracleType.Number).Value = param_id.Value;
            cmd.ExecuteNonQuery();
            return "Pedido eliminado correctamente";
        }
        [WebMethod(BufferResponse = true)]
        public string Insertar_Pedido(string origen, string puntoint, string dest, int usu, int conductora)

        {
            Oracle_conection conn = new Oracle_conection();
            conn.EstablecerConnection();

            OracleParameter param_origen = new OracleParameter();
            param_origen.OracleType = OracleType.VarChar;
            param_origen.Value = origen;

            OracleParameter param_puntointermedio = new OracleParameter();
            param_puntointermedio.OracleType = OracleType.VarChar;
            param_puntointermedio.Value = puntoint;

            OracleParameter param_destino = new OracleParameter();
            param_destino.OracleType = OracleType.VarChar;
            param_destino.Value = dest;

            OracleParameter param_idusuario = new OracleParameter();
            param_idusuario.OracleType = OracleType.Number;
            param_idusuario.Value = usu;

            OracleParameter param_idconductora = new OracleParameter();
            param_idconductora.OracleType = OracleType.Number;
            param_idconductora.Value = conductora;


            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn.GetConexion();
            cmd.CommandText = "Insertar_Pedido";
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            //cmd.Parameters.Add("P1", OracleType.Number).Value = param_id.Value;
            cmd.Parameters.Add("orig", OracleType.VarChar).Value = param_origen.Value;
            cmd.Parameters.Add("puntoi", OracleType.VarChar).Value = param_puntointermedio.Value;
            cmd.Parameters.Add("dest", OracleType.VarChar).Value = param_destino.Value;
            cmd.Parameters.Add("idua", OracleType.VarChar).Value = param_idusuario.Value;
            cmd.Parameters.Add("idcond", OracleType.VarChar).Value = param_idconductora.Value;
            cmd.ExecuteNonQuery();
            return "Pedido insertado correctamente";

        }
    }
}

