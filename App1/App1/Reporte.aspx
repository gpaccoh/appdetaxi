﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Reporte.aspx.cs" Inherits="App1.Reporte" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <link href="StyleSheet1.css" rel="stylesheet" type="text/css">
    <h2 class="titulo_pedidos"><%: Title %>.</h2>
    
    <div >
        
    <asp:GridView ID="GridView1" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" runat="server" AutoGenerateColumns="false" DataKeyNames="idpedido"
OnRowDataBound="OnRowDataBound" OnRowEditing="OnRowEditing" OnRowCancelingEdit="OnRowCancelingEdit"
OnRowUpdating="OnRowUpdating" OnRowDeleting="OnRowDeleting" RowDataBound="GridView_RowDataBound" EmptyDataText="No records has been added.">
<Columns>
    
    <asp:TemplateField HeaderText="Id" ItemStyle-Width="150" Visible="False">
        
        <ItemTemplate>
            <asp:Label ID="lblId" runat="server" Text='<%# Eval("IDPEDIDO") %>' Visible="False"></asp:Label>
        </ItemTemplate>
        
        <EditItemTemplate>
            <asp:TextBox ID="txtId" runat="server" Text='<%# Eval("IDPEDIDO") %>' Visible="False"></asp:TextBox>
        </EditItemTemplate>

    </asp:TemplateField>
    
    
    <asp:TemplateField HeaderText="Origen" ItemStyle-Width="150">
        <ItemTemplate>
            <asp:Label ID="lblNombre" runat="server" Text='<%# Eval("ORIGEN") %>'></asp:Label>
        </ItemTemplate>

        <EditItemTemplate>
            <asp:TextBox ID="txtOrigen" runat="server" Text='<%# Eval("ORIGEN") %>'></asp:TextBox>
        </EditItemTemplate>
    
    </asp:TemplateField>
    

    <asp:TemplateField HeaderText="Puntointermedio" ItemStyle-Width="150">

        <ItemTemplate>
            <asp:Label ID="lblPuntointermedio" runat="server" Text='<%# Eval("PUNTOINTERMEDIO") %>'></asp:Label>
        </ItemTemplate>


        <EditItemTemplate>
            <asp:TextBox ID="txtPuntointermedio" runat="server" Text='<%# Eval("PUNTOINTERMEDIO") %>'></asp:TextBox>
        </EditItemTemplate>

    </asp:TemplateField>


    <asp:TemplateField HeaderText="Destino" ItemStyle-Width="150">
        <ItemTemplate>
            <asp:Label ID="lblDestino" runat="server" Text='<%# Eval("DESTINO") %>'></asp:Label>
        </ItemTemplate>
        <EditItemTemplate>
            <asp:TextBox ID="txtDestino" runat="server" Text='<%# Eval("DESTINO") %>'></asp:TextBox>
        </EditItemTemplate>
    
    </asp:TemplateField>


    <asp:TemplateField HeaderText="Idusuario" ItemStyle-Width="150">
        <ItemTemplate>
            <asp:Label ID="lblUsuario" runat="server" Text='<%# Eval("IDUSUARIO") %>'></asp:Label>
        </ItemTemplate>
        <EditItemTemplate>
            <asp:TextBox ID="txtUsuario" runat="server" Text='<%# Eval("IDUSUARIO") %>'></asp:TextBox>
        </EditItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField HeaderText="Idconductora" ItemStyle-Width="150">
        <ItemTemplate>
            <asp:Label ID="lblIdconductora" runat="server" Text='<%# Eval("IDCONDUCTORA") %>'></asp:Label>
        </ItemTemplate>
        <EditItemTemplate>
            <asp:TextBox ID="txtIdconductora" runat="server" Text='<%# Eval("IDCONDUCTORA") %>'></asp:TextBox>
        </EditItemTemplate>
    </asp:TemplateField>


    <asp:CommandField ButtonType="button" ShowEditButton="true" ShowDeleteButton="true" ItemStyle-Width="150"/>

</Columns>
       
</asp:GridView>
        <br/>
</div>
<div>
<table CssClass="tablas" style="margin: 0 auto;" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse  ">

<tr>
    <td style="width: 150px">
        Origen:<br />
        <asp:TextBox ID="TextOrigen" runat="server" Width="140" />
    </td>
</tr>

<tr>
    <td style="width: 150px">
        Puntointermedio:<br />
        <asp:TextBox ID="TextPuntointermedio" runat="server" Width="140" />
    </td>
</tr>

<tr>
    <td style="width: 150px">
        Destino:<br />
        <asp:TextBox ID="TextDestino" runat="server" Width="140" />
    </td>
</tr>


<tr>
    <td style="width: 150px">
        Idusuario:<br />
        <asp:TextBox ID="TextIdusuario" runat="server" Width="140" />
    </td>
</tr>



<tr>
    <td style="width: 150px">
        Idconductora:<br />
        <asp:TextBox ID="TextIdconductora" runat="server" Width="140" />
    </td>

</tr>


<tr>
    
    <td style="width: 100px">
        <br/>
        <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="Insert" />
    </td>
</tr>
</table>        
     </div>  

</asp:Content>