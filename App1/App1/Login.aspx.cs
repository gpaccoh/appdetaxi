﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data.Odbc;
using System.Configuration;
using System.Globalization;

namespace App1
{
    public partial class Login : System.Web.UI.Page
    {
        App1.ServiceReference1.WebService1SoapClient obj = new App1.ServiceReference1.WebService1SoapClient();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void validar_Click(object sender, EventArgs e)
        {
            string usuario = v_usuario.Text;
            string contraseña = v_contra.Text;

            int num = obj.Login(usuario, contraseña);
            if (num == 1)
            {
                Response.Redirect("About.aspx");
            }
            else
            {
                Response.Write("<script>alert('Contraseña Incorrecta')</script>");
            }
        }
    }
}