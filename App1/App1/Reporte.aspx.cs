﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data.Odbc;
using System.Configuration;
using System.Globalization;

namespace App1
{
    public partial class Reporte : Page
    {
        App1.ServiceReference1.WebService1SoapClient obj = new App1.ServiceReference1.WebService1SoapClient();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.BindGrid();
            }
        }
        protected void btnList_Click(object sender, EventArgs e)
        {
            string final;
            final = obj.Pedidos_Select_Full();
            DataTable ds = JObject.Parse(final)["Table"].ToObject<DataTable>();

        }

        protected void BindGrid()
        {
            string final;
            final = obj.Pedidos_Select_Full();
            DataTable ds = JObject.Parse(final)["Table"].ToObject<DataTable>();
            GridView1.DataSource = ds;
            GridView1.DataBind();

        }


        protected void Insert(object sender, EventArgs e)
        {
            string origen = TextOrigen.Text;
            string puntoint = TextPuntointermedio.Text;
            string dest = TextDestino.Text;
            int  usu = Int32.Parse(TextIdusuario.Text);
            int conductora = Int32.Parse(TextIdconductora.Text);
            obj.Insertar_Pedido(origen, puntoint, dest, usu, conductora);
            this.BindGrid();

        }


        protected void OnRowUpdating(object sender, GridViewUpdateEventArgs e)
        {/*
            /*GridViewRow row = GridView1.Rows[e.RowIndex];
            int id = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Values[0]);
            //int id = 0;
            string origen = (row.FindControl("txtOrigen") as TextBox).Text;
            string puntoint = (row.FindControl("txtPuntointermedio") as TextBox).Text;
            string dest = (row.FindControl("txtDestino") as TextBox).Text;
            int usu = (row.FindControl("txtIdusuario") as TextBox).Text;
            int  conductora = (row.FindControl("txtIdconductora") as TextBox).Text;
         
            obj.Actual(id, nombre, apellido, direccion, telefono, correo);

            GridView1.EditIndex = -1;
            this.BindGrid();*/
        }

        protected void OnRowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            this.BindGrid();
        }
        protected void OnRowCancelingEdit(object sender, EventArgs e)
        {
            GridView1.EditIndex = -1;
            this.BindGrid();
        }
        protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int id = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Values[0]);
            obj.Eliminar_Pedido(id);
            this.BindGrid();
        }


        protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
        {



        }
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            e.Row.Cells[0].Visible = false;
        }
    }
}