﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data.Odbc;
using System.Configuration;
using System.Globalization;

namespace App1
{
    public partial class About : Page
    {
        App1.ServiceReference1.WebService1SoapClient obj = new App1.ServiceReference1.WebService1SoapClient();

        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.BindGrid();
            }
        }
        protected void btnList_Click(object sender, EventArgs e)
        {
            string final;
            final = obj.Usuario_Select_Full();
            DataTable ds = JObject.Parse(final)["Table"].ToObject<DataTable>();

        }

        protected void BindGrid()
        {
            string final;
            final = obj.Usuario_Select_Full();
            DataTable ds = JObject.Parse(final)["Table"].ToObject<DataTable>();
            GridView1.DataSource = ds;
            GridView1.DataBind();

        }
      

        protected void Insert(object sender, EventArgs e)
        {
            string nombre = TextNombre.Text;
            string apellido = TextApellido.Text;
            string direccion = TextDireccion.Text;
            string correo = TextCorreo.Text;
            string cel = TextTelefono.Text;
            int telefono = Int32.Parse(cel);
            obj.Insertar_Usuario(nombre, apellido, direccion,telefono,correo);
            this.BindGrid();
           
        }
       
        
        protected void OnRowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = GridView1.Rows[e.RowIndex];
            int id = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Values[0]);
            //int id = 0;
            string nombre = (row.FindControl("txtname") as TextBox).Text;
            string apellido = (row.FindControl("txtApellido") as TextBox).Text;
            string cel = (row.FindControl("txtTelefono") as TextBox).Text;
            string direccion = (row.FindControl("txtDireccion") as TextBox).Text;
            string correo = (row.FindControl("txtCorreo") as TextBox).Text;
            int telefono = Int32.Parse(cel);
            obj.Actualizar_Usuario( id, nombre, apellido, direccion, telefono, correo);

            GridView1.EditIndex = -1;
            this.BindGrid();
        }

        protected void OnRowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            this.BindGrid();
        }
        protected void OnRowCancelingEdit(object sender, EventArgs e)
        {
            GridView1.EditIndex = -1;
            this.BindGrid();
        }
        protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int id= Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Values[0]);
            obj.Eliminar_Usuario(id);
            this.BindGrid();
        }


        protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
        {

            

        }
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            e.Row.Cells[0].Visible = false;
        }
    }
}