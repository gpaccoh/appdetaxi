﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="App1.About" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <link href="StyleSheet1.css" rel="stylesheet" type="text/css">
    <h2 class="titulo_client"><%: Title %>.</h2>
    
    <div >
        
    <asp:GridView ID="GridView1" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" runat="server" AutoGenerateColumns="false" DataKeyNames="idusuario"
OnRowDataBound="OnRowDataBound" OnRowEditing="OnRowEditing" OnRowCancelingEdit="OnRowCancelingEdit"
OnRowUpdating="OnRowUpdating" OnRowDeleting="OnRowDeleting" RowDataBound="GridView_RowDataBound" EmptyDataText="No records has been added.">
<Columns>
    
    <asp:TemplateField HeaderText="Id" ItemStyle-Width="150" Visible="False">
        
        <ItemTemplate>
            <asp:Label ID="lblId" runat="server" Text='<%# Eval("IDUSUARIO") %>' Visible="False"></asp:Label>
        </ItemTemplate>
        
        <EditItemTemplate>
            <asp:TextBox ID="txtId" runat="server" Text='<%# Eval("IDUSUARIO") %>' Visible="False"></asp:TextBox>
        </EditItemTemplate>

    </asp:TemplateField>
    
    
    <asp:TemplateField HeaderText="Nombre" ItemStyle-Width="150">
        <ItemTemplate>
            <asp:Label ID="lblNombre" runat="server" Text='<%# Eval("NOMBRE_USUARIO") %>'></asp:Label>
        </ItemTemplate>

        <EditItemTemplate>
            <asp:TextBox ID="txtNombre" runat="server" Text='<%# Eval("NOMBRE_USUARIO") %>'></asp:TextBox>
        </EditItemTemplate>
    
    </asp:TemplateField>
    

    <asp:TemplateField HeaderText="Apellido" ItemStyle-Width="150">

        <ItemTemplate>
            <asp:Label ID="lblApellido" runat="server" Text='<%# Eval("APELLIDO_USUARIO") %>'></asp:Label>
        </ItemTemplate>


        <EditItemTemplate>
            <asp:TextBox ID="txtApellido" runat="server" Text='<%# Eval("APELLIDO_USUARIO") %>'></asp:TextBox>
        </EditItemTemplate>

    </asp:TemplateField>


    <asp:TemplateField HeaderText="Direccion" ItemStyle-Width="150">
        <ItemTemplate>
            <asp:Label ID="lblDireccion" runat="server" Text='<%# Eval("DIRECCION_USUARIO") %>'></asp:Label>
        </ItemTemplate>
        <EditItemTemplate>
            <asp:TextBox ID="txtDireccion" runat="server" Text='<%# Eval("DIRECCION_USUARIO") %>'></asp:TextBox>
        </EditItemTemplate>
    
    </asp:TemplateField>


    <asp:TemplateField HeaderText="Telefono" ItemStyle-Width="150">
        <ItemTemplate>
            <asp:Label ID="lblTelefono" runat="server" Text='<%# Eval("TELEFONO_USUARIO") %>'></asp:Label>
        </ItemTemplate>
        <EditItemTemplate>
            <asp:TextBox ID="txtTelefono" runat="server" Text='<%# Eval("TELEFONO_USUARIO") %>'></asp:TextBox>
        </EditItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField HeaderText="Correo" ItemStyle-Width="150">
        <ItemTemplate>
            <asp:Label ID="lblCorreo" runat="server" Text='<%# Eval("CORREO_USUARIO") %>'></asp:Label>
        </ItemTemplate>
        <EditItemTemplate>
            <asp:TextBox ID="txtCorreo" runat="server" Text='<%# Eval("CORREO_USUARIO") %>'></asp:TextBox>
        </EditItemTemplate>
    </asp:TemplateField>


    <asp:CommandField ButtonType="button" ShowEditButton="true" ShowDeleteButton="true" ItemStyle-Width="150"/>

</Columns>
       
</asp:GridView>
        <br/>
</div>
<div>
<table CssClass="tablas" style="margin: 0 auto;" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse  ">

<tr>
    <td style="width: 150px">
        Nombre:<br />
        <asp:TextBox ID="TextNombre" runat="server" Width="140" />
    </td>
</tr>

<tr>
    <td style="width: 150px">
        Apellido:<br />
        <asp:TextBox ID="TextApellido" runat="server" Width="140" />
    </td>
</tr>

<tr>
    <td style="width: 150px">
        Direccion:<br />
        <asp:TextBox ID="TextDireccion" runat="server" Width="140" />
    </td>
</tr>


<tr>
    <td style="width: 150px">
        Telefono:<br />
        <asp:TextBox ID="TextTelefono" runat="server" Width="140" />
    </td>
</tr>



<tr>
    <td style="width: 150px">
        Correo:<br />
        <asp:TextBox ID="TextCorreo" runat="server" Width="140" />
    </td>

</tr>


<tr>
    
    <td style="width: 100px">
        <br/>
        <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="Insert" />
    </td>
</tr>
</table>        
     </div>  

</asp:Content>